#include <iostream>
#include <cstdio>
#include <string>
#include <stdint.h>
#include <unistd.h>
#include <cstdlib>
#include <algorithm>

#include "ext3/Ext3DataParser.h"
#include "ext3/ext3_ds.h"
#include "DiskDumper.h"
#include "Bitmap.h"


enum  ProgMode {
  DUMP, RESTORE, BACKUP, PRINT_USED_BLK
};

struct RunConfig {
  ProgMode mode;
  std::string img_name;
  long int st;
  long int end;
};

RunConfig handleArguments(int a_cnt, char **args) {
  if (a_cnt < 2) {
    std::cerr << "Name of file with image must be specified" << std::endl;
    exit(-1);
  }

  RunConfig cfg;
  cfg.img_name = args[a_cnt - 1];
  cfg.mode = DUMP;

  int opt;
  while ((opt = getopt(a_cnt, args, "u:rb")) != -1) {
    switch (opt) {
    case 'u': {
      ensure(cfg.mode == DUMP, "Multiple mode are set via opts");
      cfg.mode = PRINT_USED_BLK;
      cfg.st = strtol(optarg, NULL, 10);
      ensure(optind < a_cnt, "No end pos provided");
      cfg.end = strtol(args[optind], NULL, 10);

      ensure(cfg.st >= 0, "Start block must be nonnegative");
      ensure(cfg.end >= 0, "End block must be nonnegative");
      break;
    }
    case 'r': {
      ensure(cfg.mode == DUMP, "Multiple mode are set via opts");
      cfg.mode = RESTORE;
      break;
    }
    case 'b': {
      ensure(cfg.mode == DUMP, "Multiple mode are set via opts");
      cfg.mode = BACKUP;
      break;
    }
    }
  }

  return cfg;
}

int main(int args_cnt, char **args) {
  RunConfig cfg = handleArguments(args_cnt, args);

  std::string img_name = cfg.img_name;
  Ext3DataParser parser;
  if (cfg.mode != RESTORE) {
    FILE *fd = fopen(img_name.c_str(), "rb");
    if (!fd) {
      std::cerr << "Unable to open provided image: " << img_name << std::endl;
      exit(-1);
    }
    parser.parse(fd);
    fclose(fd);
  }

  unsigned blk_sz = parser.blockSize();
  Bitmap usd_blks = parser.usedBlocks();

  switch (cfg.mode) {
  case DUMP: {
    DiskDumper dumper;
    dumper.dump(img_name, img_name + "_dumped", blk_sz, usd_blks);
    break;
  }
  case RESTORE: {
    blk_sz = usd_blks.restore(img_name + ".bm");
    DiskDumper dumper(false, true);
    dumper.dump(img_name + ".bu", img_name, blk_sz, usd_blks);
    break;
  }
  case BACKUP: {
    usd_blks.dump(img_name + ".bm", blk_sz);
    DiskDumper dumper(true, false);
    dumper.dump(img_name, img_name + ".bu", blk_sz, usd_blks);
    break;
  }
  case PRINT_USED_BLK: {
    Bitmap bm = parser.usedBlocks();
    if (!cfg.end) { cfg.end = bm.cardinality(); }
    cfg.end = std::min((uint64_t)cfg.end, bm.cardinality());
    for (int64_t blk_n = cfg.st; blk_n <= cfg.end; ++blk_n) {
      if (bm.test(blk_n)) {
        std::cout << blk_n << std::endl;
      }
    }
    break;
  }
  }
}
