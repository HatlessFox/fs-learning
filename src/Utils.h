#ifndef __FSBU_UTILS_H__
#define __FSBU_UTILS_H__

#include <iostream>
#include <cstdlib>

#define DIV_ROUND_UP(x, y) ((x)+(y)-1) / (y)

static inline void ensure(bool assmptn, char const * err_msg) {
  if (!assmptn) {
    std::cerr << "\E[31;1mAssumption Failure:\E[32;0m " <<err_msg <<std::endl;
    exit(-1);
  }
}

#endif
