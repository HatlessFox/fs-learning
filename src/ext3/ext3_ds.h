#ifndef __EXT3_STRUCTURES__
#define __EXT3_STRUCTURES__

struct ext3_reduced_sb {
  uint32_t sb_inodes_count;
  uint32_t sb_blk_cnt;
  uint32_t nonimportant1[3];
  uint32_t sb_group_start_blck;
  uint32_t sb_blk_size;
  uint32_t sb_frag_size;
  // 0x20
  uint32_t sb_blcks_per_group;
  uint32_t sb_frags_per_group;
  uint32_t padding[2];
  //0x30
  uint32_t padding_[2];
  uint16_t sb_fs_magic;
  uint16_t padd_2b[3];
  //0x40
  uint32_t padding__[6];
  //0x58
  uint16_t sb_inode_str_size;
  uint16_t sb_bg_sb_belongs_to;
  uint32_t sb_comp_ftr_flags;
  //0x60
  uint32_t sb_incomp_ftr_flags;
  uint32_t sb_readonly_ftr_flags;

  //TODO: add other usefull fields
};

// based on ext3.h:100
struct ext3_gdt_entry {
  uint32_t ge_blk_bitmap;
  uint32_t ge_inode_bitmap;
  uint32_t ge_inode_table;
  uint16_t ge_free_blocks_cnt;
  uint16_t ge_free_idodes_cnt;
  uint16_t ge_uned_dirs_cnt;
  uint16_t _padding;
  uint32_t _reserved[3];
};


#endif
