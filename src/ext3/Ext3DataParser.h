#ifndef __EXT3_DATAPARSER_H__
#define __EXT3_DATAPARSER_H__

#include <cstring>
#include <cstdio>
#include <iostream>
#include "ext3_ds.h"
#include "../Bitmap.h"
#include "../Utils.h"

const int EXT3_FS_MAGIC = 0xEF53;

class Ext3DataParser {
public: // methods
  unsigned parseSuperBlock(char *sb) {
    //NB: do we need support for restoring node frome bakcup?
    m_sblk = (ext3_reduced_sb *)sb;

    ensure(m_sblk->sb_fs_magic == EXT3_FS_MAGIC,
           "FS on image is not from ext family");

    m_blk_size = 1 << (10 + m_sblk->sb_blk_size);
    m_gr_size = m_sblk->sb_blcks_per_group;

    // group size is defined by number of bits in group's bitmap block
    // assuming bitmap takes one blck to store
    ensure(m_gr_size == m_blk_size << 3,
           "Group size must be size of block times 8");

    m_gr_cnt = DIV_ROUND_UP(
      m_sblk->sb_blk_cnt - m_sblk->sb_group_start_blck,
      m_gr_size);

    return m_blk_size;
  }

  void printSbSummary() {
    std::cout << "Common FS Info" << std::endl;
    std::cout << "Blocks #: " << m_sblk->sb_blk_cnt << "; ";
    std::cout << "Inodes #: " << m_sblk->sb_inodes_count << std::endl;

    std::cout << "Common Group Info" << std::endl;
    std::cout << "Blocks per Group: " << m_sblk->sb_blcks_per_group << "; ";
    std::cout << std::endl;

    std::cout << "Groups Count: " << m_gr_cnt << std::endl;
  }

  void printGDT(ext3_gdt_entry *data) {
    std::cout << "=========== GDT ==========" << std::endl;
    for (unsigned i = 0; i < m_gr_cnt; ++i) {
      std::cout << "Group " << i << " -- ";
      std::cout << "BlockBM:" << data->ge_blk_bitmap << "; ";
      std::cout << "InodeBM:" << data->ge_inode_bitmap << "; ";
      std::cout << "InodeTable: " << data->ge_inode_table << "; ";
      std::cout << std::endl;

      data += 1;
    }
  }

  void determineUsedBlocks(FILE *fd, ext3_gdt_entry *gdt) {
    m_bm.set(0);

    //FIXME: what if GDT takes more than one block
    char *bm_data = new char[m_blk_size];
    //NB: see actual impl of BM reading in ext3/balloc.c:147
    for (unsigned gr_ind = 0; gr_ind < m_gr_cnt; ++gr_ind) {
#ifdef EXT3_PRSNG_LOG_DEBUG
      std::cout << "+++ Analysing Group " << gr_ind;
      std::cout << (doesContainSb(gr_ind) ? " with SbBackup" : "") << std::endl;
#endif
      long int bm_blk_num = gdt[gr_ind].ge_blk_bitmap;

      bzero(bm_data, m_blk_size);
      fseek(fd, bm_blk_num * m_blk_size, SEEK_SET);
      fread(bm_data, 1, m_blk_size, fd);

      // Handle bitmap of current block

      unsigned start = m_sblk->sb_group_start_blck + m_gr_size * gr_ind;
      //FIXME: handle end overflow for last group
      unsigned ttl_blk_cnt = m_sblk->sb_blk_cnt;
      unsigned end = start + (ttl_blk_cnt <= start + m_gr_size ?
                    (ttl_blk_cnt - start) : m_gr_size);
#ifdef EXT3_PRSNG_LOG_DEBUG
      std::cout << "Block Bounds: [" << start << "; ";
      std::cout << end << ")";
      std::cout << std::endl;
#endif
      unsigned used_blocks = m_bm.setInterval(bm_data, start, end);
      unsigned free_blocks = gdt[gr_ind].ge_free_blocks_cnt;
#ifdef EXT3_PRSNG_LOG_DEBUG
      std::cout << "Bitmap block num: " << bm_blk_num << std::endl;
      std::cout << "Used blocks (from BM): " << used_blocks;
      std::cout << "; Free blocks (from GDT): " << free_blocks;
      std::cout << "   " << used_blocks + free_blocks << std::endl;
      std::cout << std::endl;
#endif
      if (end - start == m_gr_size) {
        ensure(used_blocks + free_blocks == m_gr_size,
               "Sum of used block (from bitmap) and free blocks (from GDT)"
               "doesn't equal group size");
      } else {
        ensure(used_blocks + free_blocks == end - start,
               "Sum of used block (from bitmap) and free blocks (from GDT)"
               "doesn't equal gpoup size "
               "_for the last truncated group_");
      }
    }
  }

  void parse(FILE * fd) {
    assert(fd);
    fseek(fd, 0, SEEK_SET);

    char buffer[1024];
    unsigned bytes_read = 0;

    // read boot code (skip it for now) [NB: 1024 is min ext3 blk size]
    bytes_read += fread(buffer, 1, 1024, fd);
    // read super block
    bytes_read += fread(buffer, 1, 1024, fd);

    unsigned blk_size = parseSuperBlock(buffer);

    if (blk_size > bytes_read) { // if super node is part of zero blk
      char *tmp = new char[blk_size - bytes_read];
      bytes_read += fread(tmp, 1, blk_size - bytes_read, fd);
      delete [] tmp;
    }

    ensure(!sbHasMetaBlockGroupFeature(),  // TODO: imlement this mode
           "Mata Block Group Feature isn't supported yet");
    //since GDT is sequential in memory and has size 1 << 5
    ext3_gdt_entry *gdt = new ext3_gdt_entry[m_gr_cnt];
    fread(gdt, sizeof(ext3_gdt_entry), m_gr_cnt, fd);

    #ifdef EXT3_PRSNG_LOG_DEBUG
      printSbSummary();
      printGDT(gdt);
    #endif

    determineUsedBlocks(fd, gdt);
    delete [] gdt;
  }

  Bitmap usedBlocks() const { return m_bm; }
  unsigned blockSize() const { return m_blk_size; }

private: //methods
  bool sbHasMetaBlockGroupFeature() {
    return m_sblk->sb_incomp_ftr_flags & 0x0010;
  }

  bool doesContainSb(unsigned group_id) {
    //NB: 0, 1 and powers of 3, 5, 7 block groups stores sb
    // ext3/balloc.c:1842

    if (!m_sblk->sb_readonly_ftr_flags & 0x01) { return true; }
    if (group_id <= 1) { return true; }

    unsigned t = 3, f = 5, s = 7;
    while (group_id < t || group_id < f || group_id < s) {
      if (group_id == t || group_id == f || group_id == s) { return true; }
      t *= 3; f *= 5; s *= 7;
    }

    return false;
  }
private: // fields
  unsigned m_blk_size;
  unsigned m_gr_size; // in blocks
  unsigned m_gr_cnt;
  Bitmap m_bm;

  ext3_gdt_entry *m_gdt;
  ext3_reduced_sb *m_sblk;
};

#endif
