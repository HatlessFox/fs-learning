#ifndef __BITMAP_H__
#define __BITMAP_H__

#include <iostream>
#include <vector>
#include <cassert>
#include <stdint.h>

#include "Utils.h"

class Bitmap {
public: // methods
 Bitmap():m_data(16), m_bits_number(0) {}

  uint64_t cardinality() const { return m_bits_number; }

  bool test(unsigned block_id) const {
    assert(block_id <= m_bits_number);
    unsigned bucket = block_id / 64;

    return 1 & (m_data[bucket] >> block_id % 64);
  }

  void set(unsigned block_id) {
    reserve(block_id);

    unsigned bucket = block_id / 64;

    m_data[bucket] |= ((uint64_t)1) << (block_id % 64);
#ifdef DBG_LOGGING
    std::cout << block_id << std::endl;
#endif
  }

  unsigned setInterval(char *bitmap_raw, unsigned st, unsigned end) {
    reserve(end);

    unsigned set_bits = 0;
    unsigned checked_blck_ind = 0;


    while (st + checked_blck_ind < end) {
      if (1 & (bitmap_raw[checked_blck_ind / 8] >> checked_blck_ind % 8)) {
        set(checked_blck_ind + st);
        ++set_bits;
      }
      ++checked_blck_ind;
    }
    return set_bits;
  }

  void dump(std::string const & fname, unsigned blk_sz) {
    FILE *bm_img = fopen(fname.c_str(), "wb");
    fwrite(&blk_sz, sizeof(blk_sz), 1, bm_img);
    fwrite(&m_bits_number, sizeof(uint64_t), 1, bm_img);

    for(size_t i = 0; i < m_data.size(); ++i) {
      fwrite(&m_data[i], sizeof(uint64_t), 1, bm_img);
    }

    fclose(bm_img);
  }

  // init bitmap from file and returns FS block size
  unsigned restore(std::string const & fname) {
    FILE *bm_img = fopen(fname.c_str(), "rb");
    unsigned blk_sz;
    fread(&blk_sz, sizeof(blk_sz), 1, bm_img);
    fread(&m_bits_number, sizeof(uint64_t), 1, bm_img);

    uint64_t bytes_to_read = DIV_ROUND_UP(m_bits_number, 64);
    uint64_t data;
    m_data.clear();
    for(size_t i = 0; i < bytes_to_read; ++i) {
      fread(&data, sizeof(data), 1, bm_img);
      m_data.push_back(data);
    }

    fclose(bm_img);
    return blk_sz;
  }

private: // fields

  void reserve(unsigned ind) {
    unsigned bucket = ind / 64;
    if (m_data.size() <= bucket) { m_data.resize(bucket + 1, 0); }
    if (ind > m_bits_number) { m_bits_number = ind; }
  }

  std::vector<uint64_t> m_data;
  uint64_t m_bits_number;
};

#endif //#ifndef __BITMAP_H__
