#ifndef __DISK_DUMPER_H__
#define __DISK_DUMPER_H__

#include <string>
#include "Bitmap.h"

class DiskDumper {
public:

 DiskDumper(bool ign_unused = false, bool src_ubsk_srl = false):
  m_ignore_unused(ign_unused), m_src_ublk_are_serial(src_ubsk_srl) {}

  void dump(std::string const & src_img_name,
            std::string const & dst_img_name,
            unsigned blk_size, Bitmap const & blks_usage_info) {

    FILE *src_img = fopen(src_img_name.c_str(), "rb");
    FILE *dst_img = fopen(dst_img_name.c_str(), "wb");

    uint8_t *block_data_buf = new uint8_t[blk_size]();

    uint64_t block_cnt = blks_usage_info.cardinality();

    for (uint64_t block_ind = 0; block_ind < block_cnt; ++block_ind) {
      bzero(block_data_buf, blk_size);

      if (blks_usage_info.test(block_ind)) {
        if (!m_src_ublk_are_serial) {
          fseek(src_img, block_ind * blk_size, SEEK_SET);
        }
        ensure(fread(block_data_buf, 1, blk_size, src_img) == blk_size,
               "Loaded bytes count doesn't match expected");
      } else {
        if (m_ignore_unused) { continue; } //skip unused blk write
      }

      ensure(fwrite(block_data_buf, 1, blk_size, dst_img) == blk_size,
             "Stored bytes count doesn't match expected");
    }

    delete [] block_data_buf;
    fclose(dst_img);
    fclose(src_img);
  }

private: // fields
  bool m_ignore_unused;
  bool m_src_ublk_are_serial;
};

#endif // #ifndef __DISK_DUMPER_H__
