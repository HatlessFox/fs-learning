cd ../
make clean
make all
if [ ! $? == 0 ]; then echo "Make error"; exit 1; fi
cp build/fs_bu playground/fs_bu
cd playground
./fs_bu -b $1
echo Backup is done
cp $1 $1.orig
echo Orig cpy is done
./fs_bu -r $1
echo Restore is done

../testing/blob_matcher.sh $1 $1.orig 1024 102400
