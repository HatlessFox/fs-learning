if [ $# -ne 4 ]
then
    echo "Checks whether blocks of given images are the same."
    echo "Usage: blob_matcher <1st_img> <2nd_img> <block_size> <block_cnt>"
    exit 0
fi

#-------------------
# Input check

img1=$1
#if [ ! -a $img1 ]; then echo "File '$img1' doesn't exist"; exit 1; fi
img2=$2
#if [ ! -a $img2 ]; then echo "File '$img2' doesn't exist"; exit 1; fi
let "bs=$3 * 1"
if [ ! $bs -ge 1024 ]; then echo "Block size must greater than 1024"; exit 1; fi
let "bcnt=$4 * 1"
if [ ! $bcnt -gt 0 ]; then echo "Block count must be positive"; exit 1; fi


if [ $[$[$bs - 1] & $bs] -ne 0 ]
then
    echo "Block size must be power of 2"
    exit 1;
fi

#-------------------
# Image comparing

if [ -d blk_diffs ]; then rm -r blk_diffs; fi
mkdir blk_diffs

for blk_ind in $(eval echo "{0..$[bcnt - 1]}")
do
  $(dd if=$img1 count=1 skip=$blk_ind bs=$bs 2>/dev/null | xxd > img1_tmp)
  $(dd if=$img2 count=1 skip=$blk_ind bs=$bs 2>/dev/null | xxd > img2_tmp)
  diff_data=$(diff img1_tmp img2_tmp)

  if [ $? -ne 0 ]; then
      echo "Difference in $blk_ind block"
      echo $diff_data > $(echo "blk_diffs/diff_$blk_ind")
  fi

  if [ $[$blk_ind % 1000] -eq 0 ]; then echo "Checked $blk_ind blocks"; fi
done

rm img1_tmp
rm img2_tmp