# -DEXT3_PRSNG_LOG_DEBUG -- enable ext3 parsing logging

all: sources
	mkdir -p build
	g++ -Wall  src/main.cpp -o fs_bu
	mv fs_bu build/fs_bu

sources: src/main.cpp src/ext3/Ext3DataParser.h src/Bitmap.h src/DiskDumper.h

clean:
	rm -fr build
